"use strict";

import Zertz from '../../../openxum-core/games/zertz/index.mjs';
import Graphics from '../../graphics/index.mjs';
import OpenXum from '../../openxum/gui.mjs';

// enums definition
const letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K'];

class Gui extends OpenXum.Gui {
  constructor(c, e, l, g) {
    super(c, e, l, g);

    this._tolerance = 15;
    this._delta_x = 0;
    this._delta_y = 0;
    this._delta_xy = 0;
    this._offset = 0;
    this._pointerX = -1;
    this._pointerY = -1;
    this._selected_coordinates = null;
    this._selected_marble = null;
    this._selected_marble_in_pool = null;
  }

// public methods
  draw() {
    this._compute_deltas();
    this._context.lineWidth = 1;

    // background
    this._context.strokeStyle = "#000000";
    this._context.fillStyle = "#6e4106";
    Graphics.board.draw_round_rect(this._context, 0, 0, this._canvas.width, this._canvas.height, 17, true, true);

    this._draw_rings();

    this._draw_pool();

    this._draw_marbles();

    this._draw_captured_marbles();

    if (this._engine.phase() === Zertz.Phase.SELECT_MARBLE_IN_POOL) {
      if (this._selected_marble_in_pool) {
        this._show_selected_marble_in_pool();
      } else {
        this._show_marble_in_pool();
      }
    }
    if (this._engine.phase() === Zertz.Phase.REMOVE_RING) {
      this._show_possible_removing_rings();
    }
    if (this._engine.phase() === Zertz.Phase.CAPTURE) {
      if (this._selected_marble) {
        this._show_possible_capturing_marbles();
      } else {
        this._show_possible_capture();
      }
    }

    // intersection
    if ((this._engine.phase() === Zertz.Phase.SELECT_MARBLE_IN_POOL && this._selected_marble_in_pool) || this._engine.phase() === Zertz.Phase.REMOVE_RING) {
      this._show_intersection();
    }
  }

  get_move() {
    let move = null;

    if (this._engine.phase() === Zertz.Phase.SELECT_MARBLE_IN_POOL) {
      move = new Zertz.Move(Zertz.MoveType.PUT_MARBLE, this._engine.current_color(), this._selected_coordinates,
        this._selected_color, null);
    } else if (this._engine.phase() === Zertz.Phase.REMOVE_RING) {
      move = new Zertz.Move(Zertz.MoveType.REMOVE_RING, this._engine.current_color(), this._selected_coordinates,
        null, null);
    } else if (this._engine.phase() === Zertz.Phase.CAPTURE) {
      const state = this._engine._get_intersection(this._selected_marble.letter(), this._selected_marble.number()).state();

      move = new Zertz.Move(Zertz.MoveType.CAPTURE, this._engine.current_color(), this._selected_coordinates,
        (state === Zertz.State.BLACK_MARBLE ? Zertz.MarbleColor.BLACK :
          (state === Zertz.State.WHITE_MARBLE ? Zertz.MarbleColor.WHITE : Zertz.MarbleColor.GREY)), this._selected_marble);
    }
    return move;
  }

  is_animate() {
    return false;
  }

  is_remote() {
    return false;
  }

  move(move, color) {
    this._manager.play();
    // TODO !!!!!
  }

  set_canvas(c) {
    super.set_canvas(c);

    this._canvas.addEventListener("click", (e) => {
      this._on_click(e);
    });
    this._canvas.addEventListener("mousemove", (e) => {
      this._on_move(e);
    });

    this.draw();
  }

  unselect() {
    this._selected_coordinates = null;
    this._selected_marble = null;
    this._selected_marble_in_pool = null;
  }

// private methods
  _compute_coordinates(letter, number) {
    return [this._offset + (letter - 'A'.charCodeAt(0)) * this._delta_x * Math.sqrt(3) / 2 + this._delta_x / 2,
      this._offset / 2 + 9 * this._delta_xy + this._delta_xy * (letter - 'A'.charCodeAt(0)) - (number - 1) * this._delta_y + this._delta_y / 2];
  }

  _compute_deltas() {
    this._offset = 30;
    this._delta_x = (this._width - 2 * this._offset) / 7.0;
    this._delta_y = this._delta_x;
    this._delta_xy = this._delta_y / 2;
    this._offset = (this._width - 7.0 * this._delta_x * Math.sqrt(3) / 2) / 2;
  }

  _compute_letter(x, y) {
    x /= Math.sqrt(3) / 2;
    x -= this._delta_x / 2;

    const index = Math.floor((x - this._offset) / this._delta_x);
    const x_ref = this._offset + this._delta_x * index;
    const x_ref_2 = this._offset + this._delta_x * (index + 1);
    let _letter = 'X';

    if (x < this._offset) {
      _letter = 'A';
    } else if (x <= x_ref + this._delta_x / 2 && x >= x_ref && x <= x_ref + this._tolerance) {
      _letter = letters[index];
    } else if (x > x_ref + this._delta_x / 2 && x >= x_ref_2 - this._tolerance) {
      _letter = letters[index + 1];
    }
    return _letter;
  }

  _compute_number(x, y) {
    x /= Math.sqrt(3) / 2;
    x -= this._delta_x / 2;
    y += this._delta_y / 2;

    const pt = this._compute_coordinates('A'.charCodeAt(0), 1);

    // translation to A1 and rotation
    const X = x - pt[0];
    const Y = y - pt[1];
    const sin_alpha = 1.0 / Math.sqrt(5);
    const cos_alpha = 2.0 * sin_alpha;

    const x2 = Math.floor((X * sin_alpha - Y * cos_alpha) + pt[0]);
    const delta_x2 = Math.floor(this._delta_x * cos_alpha);

    const index = Math.floor((x2 - this._offset) / delta_x2);
    const x_ref = Math.floor(this._offset + delta_x2 * index);
    const x_ref_2 = Math.floor(this._offset + delta_x2 * (index + 1));

    let _number = -1;

    if (x2 > 0 && x2 < this._offset) {
      _number = 1;
    } else if (x2 <= x_ref + delta_x2 / 2 && x2 >= x_ref && x2 <= x_ref + this._tolerance) {
      _number = index + 1;
    } else if (x2 > x_ref + delta_x2 / 2 && x2 >= x_ref_2 - this._tolerance) {
      _number = index + 2;
    }
    return _number;
  }

  _compute_pointer(x, y) {
    let change = false;
    let letter = this._compute_letter(x, y);

    if (letter !== 'X') {
      let number = this._compute_number(x, y);

      if (number !== -1) {
        if (this._engine.exist_intersection(letter, number)) {
          let pt = this._compute_coordinates(letter.charCodeAt(0), number);

          this._pointerX = pt[0];
          this._pointerY = pt[1];
          change = true;
        } else {
          this._pointerX = this._pointerY = -1;
          change = true;
        }
      } else {
        if (this._pointerX !== -1) {
          this._pointerX = this._pointerY = -1;
          change = true;
        }
      }
    } else {
      if (this._pointerX !== -1) {
        this._pointerX = this._pointerY = -1;
        change = true;
      }
    }
    return change;
  }

  _draw_captured_marbles_of(player) {
    let i = 0;
    let l = 0;
    let c = 0;
    const nc = [ 8, 7, 5, 3, 1 ];

    while (i < this._engine._capturedBlackMarbleNumber[player]) {
      if (player === Zertz.Color.ONE) {
        Graphics.marble.draw_marble(this._context, 20 + 30 * c, this._height - (20 + 30 * l), 25, "black");
      } else {
        Graphics.marble.draw_marble(this._context, this._width - (20 + 30 * c), this._height - (20 + 30 * l), 25, "black");
      }
      ++c;
      ++i;
      if (c === nc[l]) {
        c = 0;
        ++l;
      }
    }

    i = 0;
    while (i < this._engine._capturedGreyMarbleNumber[player]) {
      if (player === Zertz.Color.ONE) {
        Graphics.marble.draw_marble(this._context, 20 + 30 * c, this._height - (20 + 30 * l), 25, "grey");
      } else {
        Graphics.marble.draw_marble(this._context, this._width - (20 + 30 * c), this._height - (20 + 30 * l), 25, Zertz.MarbleColor.GREY);
      }
      ++c;
      ++i;
      if (c === nc[l]) {
        c = 0;
        ++l;
      }
    }

    i = 0;
    while (i < this._engine._capturedWhiteMarbleNumber[player]) {
      if (player === Zertz.Color.ONE) {
        Graphics.marble.draw_marble(this._context, 20 + 30 * c, this._height - (20 + 30 * l), 25, "white");
      } else {
        Graphics.marble.draw_marble(this._context, this._width - (20 + 30 * c), this._height - (20 + 30 * l), 25, "white");
      }
      ++c;
      ++i;
      if (c === nc[l]) {
        c = 0;
        ++l;
      }
    }
  }

  _draw_captured_marbles() {
    this._draw_captured_marbles_of(Zertz.Color.ONE);
    this._draw_captured_marbles_of(Zertz.Color.TWO);
  }

  _draw_marbles() {
    for (let index in this._engine._intersections) {
      const intersection = this._engine._intersections[index];
      const state = intersection.state();

      if (state !== Zertz.State.EMPTY) {
        if (state === Zertz.State.BLACK_MARBLE || state === Zertz.State.GREY_MARBLE || state === Zertz.State.WHITE_MARBLE) {
          const pt = this._compute_coordinates(intersection.letter().charCodeAt(0), intersection.number());

          Graphics.marble.draw_marble(this._context, pt[0], pt[1], 2 * this._delta_x / 3.0,
            intersection.color() === Zertz.MarbleColor.BLACK ? "black" : intersection.color() === Zertz.MarbleColor.WHITE ? "white" : "grey");
        }
      }
    }
  }

  _draw_pool() {
    let i = 0;
    let l = 0;
    let c = 0;
    const nc = [8, 7, 5, 3, 1];

    while (i < this._engine._blackMarbleNumber) {
      Graphics.marble.draw_marble(this._context, 20 + 30 * c, 20 + 30 * l, 25, "black");
      ++c;
      ++i;
      if (c === nc[l]) {
        c = 0;
        ++l;
      }
    }

    i = 0;
    while (i < this._engine._greyMarbleNumber) {
      Graphics.marble.draw_marble(this._context, 20 + 30 * c, 20 + 30 * l, 25, "grey");
      ++c;
      ++i;
      if (c === nc[l]) {
        c = 0;
        ++l;
      }
    }

    i = 0;
    while (i < this._engine._whiteMarbleNumber) {
      Graphics.marble.draw_marble(this._context, 20 + 30 * c, 20 + 30 * l, 25, "white");
      ++c;
      ++i;
      if (c === nc[l]) {
        c = 0;
        ++l;
      }
    }
  }

  _draw_ring(x, y) {
    let gr = this._context.createLinearGradient(x, y, x + this._delta_x / 3.0, y + this._delta_x / 3.0);

    this._context.beginPath();
    gr.addColorStop(0, '#008000');
    gr.addColorStop(1, '#98FB98');
    this._context.fillStyle = gr;
    this._context.arc(x, y, this._delta_x / 2.0 - 1, 0.0, 2 * Math.PI, false);
    this._context.fill();
    this._context.closePath();

    this._context.lineWidth = 1;
    this._context.strokeStyle = "#000000";
    this._context.beginPath();
    this._context.arc(x, y, this._delta_x / 2.0 - 1, 0.0, 2 * Math.PI, false);
    this._context.stroke();
    this._context.closePath();

    this._context.fillStyle = "#6e4106";
    this._context.beginPath();
    this._context.arc(x, y, this._delta_x / 5, 0.0, 2 * Math.PI, false);
    this._context.fill();
    this._context.stroke();
    this._context.closePath();
  }

  _draw_rings() {
    for (let index in this._engine._intersections) {
      const intersection = this._engine._intersections[index];

      if (intersection.state() !== Zertz.State.EMPTY) {
        const pt = this._compute_coordinates(intersection.letter().charCodeAt(0), intersection.number());

        this._draw_ring(pt[0], pt[1]);
      }
    }
  }

  _find_marble_color(ref_l, ref_c) {
    let i = 0;
    let l = 0;
    let c = 0;
    const nc = [8, 7, 5, 3, 1];

    while (i < this._engine._blackMarbleNumber) {
      if (ref_l === l && ref_c === c) {
        return Zertz.MarbleColor.BLACK;
      }
      ++c;
      ++i;
      if (c === nc[l]) {
        c = 0;
        ++l;
      }
    }

    i = 0;
    while (i < this._engine._greyMarbleNumber) {
      if (ref_l === l && ref_c === c) {
        return Zertz.MarbleColor.GREY;
      }
      ++c;
      ++i;
      if (c === nc[l]) {
        c = 0;
        ++l;
      }
    }

    i = 0;
    while (i < this._engine._whiteMarbleNumber) {
      if (ref_l === l && ref_c === c) {
        return Zertz.MarbleColor.WHITE;
      }
      ++c;
      ++i;
      if (c === nc[l]) {
        c = 0;
        ++l;
      }
    }
  }

  _get_click_position(e) {
    let rect = this._canvas.getBoundingClientRect();

    return {x: (e.clientX - rect.left) * this._scaleX, y: (e.clientY - rect.top) * this._scaleY};
  }

  _on_click(event) {
    if (this._engine.current_color() === this._color || this._gui) {
      const pos = this._get_click_position(event);

      if (this._engine.phase() === Zertz.Phase.SELECT_MARBLE_IN_POOL && this._selected_marble_in_pool === null) {
        if (this._pointerX !== -1 && this._pointerY !== -1) {
          this._selected_marble_in_pool = {x: this._pointerX, y: this._pointerY};
        }
      } else {
        const letter = this._compute_letter(pos.x, pos.y);

        if (letter !== 'X') {
          const number = this._compute_number(pos.x, pos.y);

          if (number !== -1) {
            let ok = false;

            if (this._engine.phase() === Zertz.Phase.SELECT_MARBLE_IN_POOL) {
              if (this._engine._get_intersection(letter, number).state() === Zertz.State.VACANT) {
                this._selected_coordinates = new Zertz.Coordinates(letter, number);
                ok = true;
              }
            } else if (this._engine.phase() === Zertz.Phase.REMOVE_RING) {
              const coordinates = new Zertz.Coordinates(letter, number);

              if (this._engine.verify_remove_ring(coordinates)) {
                this._selected_coordinates = coordinates;
                ok = true;
              }
            } else if (this._engine.phase() === Zertz.Phase.CAPTURE) {
              const coordinates = new Zertz.Coordinates(letter, number);

              if (this._selected_marble && this._engine.is_possible_capturing_marble_with(this._selected_marble, coordinates)) {
                this._selected_coordinates = coordinates;
                ok = true;
              } else {
                if (this._engine.is_possible_to_capture_with(this._engine._get_intersection(letter, number))) {
                  this._selected_marble = coordinates;
                }
              }
            }
            if (ok) {
              this._manager.play();
            }
          }
        }
      }
    }
  }

  _on_move(event) {
    if (this._engine.current_color() === this._color || this._gui) {
      const pos = this._get_click_position(event);

      if (this._engine.phase() === Zertz.Phase.SELECT_MARBLE_IN_POOL && this._selected_marble_in_pool === null) {
        const l = Math.floor((pos.y - 8.5) / 30);

        if (l < 6) {
          const c = Math.floor(pos.x / 30);
          const nc = [8, 7, 5, 3, 1];

          if (c < nc[l]) {
            let n = 0;

            for (let i = 0; i < l; ++i) {
              n += nc[i];
            }
            n += c + 1;
            if (n <= this._engine._blackMarbleNumber + this._engine._greyMarbleNumber + this._engine._whiteMarbleNumber) {
              this._selected_color = this._find_marble_color(l, c);
              this._pointerX = 20 + 30 * c;
              this._pointerY = 20 + 30 * l;
              this._manager.redraw();
            }
          } else {
            if (this._pointerX !== -1 && this._pointerY !== -1) {
              this._pointerX = -1;
              this._pointerY = -1;
              this._manager.redraw();
            } else {
              this._pointerX = -1;
              this._pointerY = -1;
            }
          }
        } else {
          if (this._pointerX !== -1 && this._pointerY !== -1) {
            this._pointerX = -1;
            this._pointerY = -1;
            this._manager.redraw();
          } else {
            this._pointerX = -1;
            this._pointerY = -1;
          }
        }
      } else {
        const change = this._pointerX !== -1 && this._pointerY !== -1;
        const letter = this._compute_letter(pos.x, pos.y);

        if (letter !== 'X') {
          const number = this._compute_number(pos.x, pos.y);

          if (number !== -1) {
            if (this._compute_pointer(pos.x, pos.y)) {
              this._manager.redraw();
            }
          } else {
            if (change) {
              this._pointerX = -1;
              this._pointerY = -1;
              this._manager.redraw();
            }
          }
        } else {
          if (change) {
            this._pointerX = -1;
            this._pointerY = -1;
            this._manager.redraw();
          }
        }
      }
    }
  }

  _show_intersection() {
    if (this._pointerX !== -1 && this._pointerY !== -1) {
      this._context.fillStyle = "#0000ff";
      this._context.strokeStyle = "#0000ff";
      this._context.lineWidth = 1;
      this._context.beginPath();
      this._context.arc(this._pointerX, this._pointerY, 5, 0.0, 2 * Math.PI);
      this._context.closePath();
      this._context.fill();
      this._context.stroke();
    }
  }

  _show_marble_in_pool() {
    if (this._pointerX !== -1 && this._pointerY !== -1) {
      this._context.fillStyle = "#0000ff";
      this._context.strokeStyle = "#0000ff";
      this._context.lineWidth = 4;
      this._context.beginPath();
      this._context.arc(this._pointerX, this._pointerY, 12.5, 0.0, 2 * Math.PI);
      this._context.closePath();
      this._context.stroke();
    }
  }

  _show_possible_capturing_marbles() {
    const list = this._engine.get_possible_capturing_marbles(this._selected_marble);

    for (let index = 0; index < list.length; ++index) {
      const pt = this._compute_coordinates(list[index].letter().charCodeAt(0), list[index].number());

      this._context.fillStyle = "#ff0000";
      this._context.strokeStyle = "#ff0000";
      this._context.lineWidth = 4;
      this._context.beginPath();
      this._context.arc(pt[0], pt[1], this._delta_x / 2.0, 0.0, 2 * Math.PI);
      this._context.closePath();
      this._context.stroke();
    }
  }

  _show_possible_capture() {
    const list = this._engine._get_can_capture_marbles();

    for (let index = 0; index < list.length; ++index) {
      const pt = this._compute_coordinates(list[index].letter().charCodeAt(0), list[index].number());

      this._context.fillStyle = "#0000ff";
      this._context.strokeStyle = "#0000ff";
      this._context.lineWidth = 4;
      this._context.beginPath();
      this._context.arc(pt[0], pt[1], this._delta_x / 2.0, 0.0, 2 * Math.PI);
      this._context.closePath();
      this._context.stroke();
    }
  }

  _show_possible_removing_rings() {
    const list = this._engine.get_possible_removing_rings();

    for (let index = 0; index < list.length; ++index) {
      const pt = this._compute_coordinates(list[index].letter().charCodeAt(0), list[index].number());

      this._context.fillStyle = "#0000ff";
      this._context.strokeStyle = "#0000ff";
      this._context.lineWidth = 4;
      this._context.beginPath();
      this._context.arc(pt[0], pt[1], this._delta_x / 2.0, 0.0, 2 * Math.PI);
      this._context.closePath();
      this._context.stroke();
    }
  };

  _show_selected_marble_in_pool() {
    if (this._selected_marble_in_pool) {
      this._context.fillStyle = "#0000ff";
      this._context.strokeStyle = "#0000ff";
      this._context.lineWidth = 4;
      this._context.beginPath();
      this._context.arc(this._selected_marble_in_pool.x, this._selected_marble_in_pool.y, 12.5, 0.0, 2 * Math.PI);
      this._context.closePath();
      this._context.stroke();
    }
  }
}

export default Gui;