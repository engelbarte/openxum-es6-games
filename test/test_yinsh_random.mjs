import OpenXum from '../lib/openxum-core';
import AI from '../lib/openxum-ai';

let e = new OpenXum.Yinsh.Engine(OpenXum.Yinsh.GameType.REGULAR, OpenXum.Yinsh.Color.BLACK);
let p1 = new AI.Generic.RandomPlayer(OpenXum.Yinsh.Color.BLACK, OpenXum.Yinsh.Color.WHITE, e);
let p2 = new AI.Generic.RandomPlayer(OpenXum.Yinsh.Color.WHITE, OpenXum.Yinsh.Color.BLACK, e);
let p = p1;
let moves = [];

while (!e.is_finished()) {
  let move = p.move();

  if (move.constructor === Array) {
    for (let i = 0; i < move.length; ++i) {
      console.log(move[i].to_string());
    }
  } else {
    console.log(move.to_string());
  }

  moves.push(move);
  e.move(move);
  p = p === p1 ? p2 : p1;
}

console.log("Winner is " + (e.winner_is() === OpenXum.Yinsh.Color.BLACK ? "black" : "white"));
for (let index = 0; index < moves.length; ++index) {
  console.log(moves[index].to_string());
}